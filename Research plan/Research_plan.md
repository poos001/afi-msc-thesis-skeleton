# Title

Be as brief as possible. If applicable, indicate if it is a pilot study.

# Background and Significance
Presentation of the problem. Provide a literature review that
sketches current knowledge of the topic, including previous work, if applicable. Evaluate
knowledge gaps, which your project could fill. State concisely the importance of the research
described in the protocol. Relate this work to longer-term research objectives.
# Bibliography

List of relevant references with at least two key publications.

# Specific Aims and scope

Main (+secondary) objective(s). What hypothesis(es) or research
question is to be tested in this protocol? What do you hope to learn from this research?

# Research Design and Methods

 Describe the research design in relation to the following points:
* Relate the design to your hypothesis(es).
* A flow chart illustrating your study design is often helpful.
* An appropriate statement should be included, describing which variables are measured,
and how and where they are to be analysed (strategy for statistical analysis, tests and
presentations, software and person(s) in charge). Appointments must be made with the
person(s) in charge of the analysis.
* In case an animal experiment is included a form is attached with information on
(number of) experimental animals, maintenance conditions, treatments tested
(reference, formulation, dosage etc), study procedure (biological procedures, time
schedule) sampling periods and observations (what is done, who does it, when).
* Identify conditions that may be hazardous (e.g. lab-conditions) and describe precautions
that will be taken to minimise risks.
* All activities, including dates for presentations and handing in of the final thesis report
are stated in a detailed time schedule.